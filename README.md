sshfs
=====

Mount an SSHFS remote path.
An entry is added to /etc/fstab using options `noauto,x-systemd.automount,_netdev,IdentityFile={{ sshfs_identity }},uid={{ uid }},gid={{ gid }},allow_other,reconnect`
where uid and gid are gathered from `sshfs_owner` and `sshfs_group`.



Requirements
------------

You must have a user and keys for the SSH connection.
Additionally, your sshfs_host must be reachable from your host where you deploy this.


Role Variables
--------------

```
sshfs_host: example.com    
sshfs_user: "{{ lookup('env','USER') }}"  # SSH login user at example.com
sshfs_remote_path: /home/{{ sshfs_user }}
sshfs_identity: "/home/{{ lookup('env','USER') }}/.ssh/id_rsa"  # must exist at the host
sshfs_mountpoint: /mnt/sshfs
sshfs_owner: "{{ lookup('env','USER') }}"  # local mountpoint owner name
sshfs_group: "{{ lookup('env','USER') }}"  # local mountpoint group name
```

Dependencies
------------

None

Example Playbook
----------------

Deploy to a VPS

    - hosts: all
      roles:
         - sshfs

License
-------

GPL

Author Information
------------------

https://gitlab.com/jfriis
